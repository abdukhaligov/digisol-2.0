(function($) {

    /*Hamburger Open and add animations*/
    function hamb() {
        $('.h-hamb-menu > .h-pointer').click(function () {
            $('.h-hamb-button.close').toggleClass("h-rotate-90");
            $('.h-hamb-button.close').toggleClass("h-opacity");
            $('.h-hamb-button.active').toggleClass("h-rotate-o90");
            $('.h-hamb-button.active').toggleClass("h-opacity");

            $('header').toggleClass("h-ofc");
            $('header').toggleClass("h-white");
            $('.h-hamb-text').toggleClass("h-ofc-t");
            $('.h-hamb-text').toggleClass("h-white-t");
            $('.h-logo').toggleClass("h-grayscale");
            $('.h-hamb').slideToggle(700);
        });

    }

    /*Hamburger open submenu*/
    function hambnavs() {
        $('.h-navs_1').click(function () {
            $(".h-navs_1").next('.h-navs-in').slideToggle(700);
            $(".h-navs_1").children('.h-plus').toggleClass('h-rotate-45');
            $(".h-navs_2").next('.h-navs-in').slideUp(700);
            $(".h-navs_2").children('.h-plus').removeClass('h-rotate-45');

        });
        $('.h-navs_2').click(function () {
            $(".h-navs_2").next('.h-navs-in').slideToggle(700);
            $(".h-navs_2").children('.h-plus').toggleClass('h-rotate-45');
            $(".h-navs_1").next('.h-navs-in').slideUp(700);
            $(".h-navs_1").children('.h-plus').removeClass('h-rotate-45');

        });
    }

    /*Animate fixed header*/
    function headersticky() {
        var temp = $('.h-header-sticky').height();
        // alert(temp);
        if (document.body.scrollTop > temp || document.documentElement.scrollTop > temp) {
            $('.h-header-sticky').slideDown();
            $('.h-totop').fadeIn();
        }
        else{
            $('.h-header-sticky').slideUp();
            $('.h-totop').fadeOut();
        }
    }

    /*To top*/
    function totop() {
        $('.h-totop').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    }

    /*Product images*/
    function products() {
        $('.h-product-sm-1').click(function () {
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-1').fadeIn(0);
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-2').fadeOut(0);
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-3').fadeOut(0);
        });
        $('.h-product-sm-2').click(function () {
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-2').fadeIn(0);
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-1').fadeOut(0);
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-3').fadeOut(0);
        });
        $('.h-product-sm-3').click(function () {
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-3').fadeIn(0);
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-2').fadeOut(0);
            $(this).parent('div').parent('div').siblings('div').children('.h-product-bg-1').fadeOut(0);
        });

    }


    $('document').ready(hamb);
    $('document').ready(hambnavs);
    $('document').ready(totop);
    $('document').ready(products());

    window.onscroll = function() {headersticky()};



    /*Swiper*/
    var swiper_0 = new Swiper('.swp-cnt-testimonial', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.swp-pg-testimonial',
            clickable: true,
        },
        speed: 750,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        }

    });
    var swiper_1 = new Swiper('.swp-cnt-brands', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.swp-pg-brands',
            clickable: true,
        },
        speed: 750,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        }

    });
    var swiper_2 = new Swiper('.swp-cnt-prod', {
        pagination: {
            el: '.swp-pg-prod',
            type: 'fraction',
        },
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });



})(jQuery);



